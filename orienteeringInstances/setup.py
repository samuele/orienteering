from __future__ import print_function
import glob, os, sys
import matlab.engine
import time

# Run this script in folder ../orienteering/orienteeringInstances

fastOP_stat = {'Points': [], 'Scores': [], 'Costs': [], 'Times': []}
New_stat = {'Points': [], 'Scores': [], 'Costs': [], 'Times': []}
Basic_stat = {'Points': [], 'Scores': [], 'Costs': [], 'Times': []}

print('Starting MATLAB engine and configure the environment...', end = '')
eng = matlab.engine.start_matlab()
eng.cd('..')
eng.config(nargout = 0)
eng.cd('orienteeringInstances')
print(' Done.')

cnt = -1
files = glob.glob('*Y.txt')
for file in files:
    cnt += 1
    print('{0:.0f}%'.format(float(cnt) / len(files) * 100))

    print('Generating from ' + file + '...', end = '')

    with open(file) as r:
        line_split = r.readline().split()
        si, sj = [int(x) for x in line_split[2:-1]]
        budget = float(line_split[-1])
        G = r.read()

    with open(os.path.splitext(file)[0] + '-sol.txt') as sol:
        line_split = sol.readline().split()
        basicScore, basicCost = [float(x) for x in line_split[1:]]
        basicPoints = int(line_split[0])

        basicPath = '['
        for line in sol:
            basicPath += line + ';'
        basicPath += '];'

        ei, ej = [int(x) for x in line.split()]

    setup_script = 'setup_' + os.path.splitext(file)[0].replace('-', '_') + '.m'
    with open(setup_script, 'w') as w:
        startNode = 'startNode = [' + str(si) + ', ' + str(sj) + '];'
        endNode = 'endNode = [' + str(ei) + ', ' + str(ej) + '];'
        budget = 'budget = ' + str(budget) + ';'
        matrixATX = 'matrixATX = [' + G + '];'

        w.write(startNode + '\n')
        w.write(endNode + '\n')
        w.write(budget + '\n')
        w.write('unitOfDistance = 1;\n')
        w.write(matrixATX)

    print(' Done.')

    print('Loading environment...', end = '')
    eng.run(setup_script, nargout = 0)
    print(' Done.')

    print('Drawing the model...', end = '')
    eng.assignin('base', 'orienteeringInstanceTitle', file, nargout = 0)
    eng.draw_matrix(nargout = 0)
    print(' Done.')


    print('Running fastOP algorithm...', end = '')
    eng.cd('..')
    start = time.time()
    output_fastOP = eng.solveOrienteeringWithFastOP(eng.eval('matrixATX', nargout = 1),
                                                    eng.eval('startNode', nargout = 1),
                                                    eng.eval('endNode', nargout = 1),
                                                    eng.eval('budget', nargout = 1),
                                                    eng.eval('unitOfDistance', nargout = 1), nargout = 4)
    end = time.time()
    print(' Done.')

    fastOP_stat['Points'].append(int(eng.len(output_fastOP[3], nargout = 1)))
    fastOP_stat['Scores'].append(eng.score(output_fastOP[3], nargout = 1))
    fastOP_stat['Costs'].append(eng.cost(output_fastOP[3], nargout = 1))
    fastOP_stat['Times'].append(end - start)

    print('Drawing path on the model...', end = '')
    eng.draw_path(output_fastOP[3], 'fastOP', nargout = 0)
    print(' Done.')

    print('Running New algorithm...', end = '')
    start = time.time()
    output_New = eng.solveOrienteering(eng.eval('matrixATX', nargout = 1),
                                       eng.eval('startNode', nargout = 1),
                                       eng.eval('endNode', nargout = 1),
                                       eng.eval('budget', nargout = 1),
                                       eng.eval('unitOfDistance', nargout = 1), nargout = 4)
    end = time.time()
    print(' Done.')

    New_stat['Points'].append(int(eng.len(output_New[3], nargout = 1)))
    New_stat['Scores'].append(eng.score(output_New[3], nargout = 1))
    New_stat['Costs'].append(eng.cost(output_New[3], nargout = 1))
    New_stat['Times'].append(end - start)

    print('Drawing path on the model...', end = '')
    eng.draw_path(output_New[3], 'New', nargout = 0)
    print(' Done.')

    print('Simulating Basic algorithm...', end = '')
    start = time.time()
    path = eng.path()
    eng.eval('basicScore = ' + str(basicScore) + ';', nargout = 0)
    eng.eval('basicCost = ' + str(basicCost) + ';', nargout = 0)
    eng.eval('basicPath = ' + basicPath + ';', nargout = 0)
    eng.initPath(path,
                 eng.eval('basicPath', nargout = 1),
                 eng.eval('basicCost', nargout = 1),
                 eng.eval('basicScore', nargout = 1), nargout = 0)
    end = time.time()
    print(' Done.')

    Basic_stat['Points'].append(basicPoints)
    Basic_stat['Scores'].append(basicScore)
    Basic_stat['Costs'].append(basicCost)
    Basic_stat['Times'].append(end - start)

    print('Drawing path on the model...', end = '')
    eng.draw_path(path, 'Basic', nargout = 0)
    print(' Done.')

    print('Saving figure and closing...', end = '')
    eng.save_and_close('./orienteeringInstances/' + os.path.splitext(file)[0] + '.png', nargout = 0)
    print(' Done.')

    eng.clear(nargout = 0)
    eng.cd('./orienteeringInstances')

print('100%\n\nStats generation...')

time.sleep(3)

percScores = []
percCosts = []

print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
print('+       Instance       ||       Points       ||            Score            ||            Cost             ||            Time             || Improvement (Score) || Improvement (Cost) +')
print('+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+')
for i in range(0, cnt + 1):
    print('+ {:>20} '.format(files[i]), end = '')
    print('|| {:4d}   {:4d}   {:4d} '.format(fastOP_stat['Points'][i], New_stat['Points'][i], Basic_stat['Points'][i]), end = '')
    print('|| {:7.2f}   {:7.2f}   {:7.2f} '.format(fastOP_stat['Scores'][i], New_stat['Scores'][i], Basic_stat['Scores'][i]), end = '')
    print('|| {:7.2f}   {:7.2f}   {:7.2f} '.format(fastOP_stat['Costs'][i], New_stat['Costs'][i], Basic_stat['Costs'][i]), end = '')
    print('|| {:7.2f}   {:7.2f}   {:7.2f} '.format(fastOP_stat['Times'][i], New_stat['Times'][i], Basic_stat['Times'][i]), end = '')

    if fastOP_stat['Scores'][i] != 0:
        percScore = ((New_stat['Scores'][i] - fastOP_stat['Scores'][i]) / fastOP_stat['Scores'][i]) * 100
        percScores.append(percScore)
        print('||      {:+7.2f}%       '.format(percScore), end = '')
    else:
        print('||          +inf       ', end = '')

    if fastOP_stat['Costs'][i] != 0:
        percCost = ((New_stat['Costs'][i] - fastOP_stat['Costs'][i]) / fastOP_stat['Costs'][i]) * 100
        percCosts.append(percCost)
        print('||      {:+7.2f}%      +'.format(percCost))
    else:
        print('||         +inf       +', end = '')

print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

print('\nFinal results generation...')
time.sleep(3)

print('+------------------------------------+')
if len(percScores) != 0:
    avgscore = sum(percScores) / len(percScores)
    print('+ Avg. score improvement    {:+7.2f}% +'.format(avgscore))
else:
    print('+ Avg. score improvement             +')

if len(percCosts) != 0:
    avgcost = sum(percCosts) / len(percCosts)
    print('+ Avg. cost improvement     {:+7.2f}% +'.format(avgcost))
else:
    print('+ Avg. cost improvement              +')

if len(New_stat['Times']) != 0:
    avgtime = sum(New_stat['Times']) / len(New_stat['Times'])
    print('+ Avg. execution time        {:6.2f}s +'.format(avgtime))
else:
    print('+ Avg. execution time                +')
print('+------------------------------------+')
