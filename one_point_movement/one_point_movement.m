function [moves, improvement] = one_point_movement(pathOP, pathsNOP, G, si, sj, ei, ej, budget, moves, improvement)

    % For i = the first to the last point in the ellipse (say point i is in path q) (A LOOP)
    %     For j = the first to the last point in the first to the last path (B LOOP)
    %       (p) in both path_op and paths_nop sets (p != q)
    %         If inserting i in front of j on path p is feasible and the total score increases,
    %             then make the movement and go to the A loop
    %     End B loop
    % End A loop
    
    allPaths(1) = pathOP;
    allPaths(2 : size(pathsNOP, 2) + 1) = pathsNOP(1 : size(pathsNOP, 2));

    scoreOP = pathOP.p{1, 3};
    
    R = size(G, 1);
    C = size(G, 2);
    
    v = 1 : R;
    shuffledRows = v(randperm(length(v))); 
    for r = shuffledRows
        
        v = 1 : C;
        shuffledCols = v(randperm(length(v)));
        for c = shuffledCols;
            
            toALoop = false;
            
            if G(r, c) == 0 || dst([r, c], [si, sj]) + dst([r, c], [ei, ej]) > budget
                continue
            end
            
            v = 1 : size(allPaths, 2);
            shuffledPaths = v(randperm(length(v)));
            for p = shuffledPaths
               
                if toALoop
                    break
                end
                
                if contains([r, c], allPaths(p).p)
                    continue
                end
                
                currentPath = allPaths(p);
                
                v = 2 : size(currentPath.p{1, 1}, 1);
                shuffledPoints = v(randperm(length(v)));
                for j = shuffledPoints
                    
                    newCost = currentPath.p{1, 2} ...
                            + dst(currentPath.p{1, 1}(j - 1, :), [r, c]) ...
                            + dst(currentPath.p{1, 1}(j, :), [r, c]) ...
                            - dst(currentPath.p{1, 1}(j - 1, :), currentPath.p{1, 1}(j, :));
                    
                    if newCost > budget
                        continue
                    end
                    
                    insert(r, c, G, j, currentPath, newCost);
                    
                    if currentPath.p{1, 3} > scoreOP
                        if p == 1
                            scoreOP = pathOP.p{1, 3};
                        else
                            pathsNOP(p - 1) = pathOP(1);
                            pathOP = currentPath;
                            scoreOP = pathOP.p{1, 3};
                        end
                    else
                         pathsNOP(p - 1) = currentPath;
                    end
                        
                    allPaths(1) = pathOP;
                    allPaths(2 : size(pathsNOP, 2) + 1) = pathsNOP(1 : size(pathsNOP, 2));
   
                    toALoop = true;
                    moves = true;
                    improvement = true;
                    break
                end
                    
            end
            
        end
    end
    
    pathsNOP(1 : size(pathsNOP, 2)) = allPaths(2 : size(allPaths, 2));   
end

function insert(r, c, G, j, currentPath, newCost)

    % Build the new path.
    currentPath.p{1, 1}(j + 1 : size(currentPath.p{1, 1}, 1) + 1, :) = currentPath.p{1, 1}(j : size(currentPath.p{1, 1}, 1), :);
    currentPath.p{1, 1}(j, :) = [r, c];
    
    % Get the new cost.
    currentPath.p{1, 2} = newCost;
    
    % Get the new score.
    currentPath.p{1, 3} = currentPath.p{1, 3} + G(r, c);
    
end
