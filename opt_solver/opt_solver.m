%% Find the optimal path of the given orienteering problem
% Explore all path (works in |O(n^n)| time) and return the optimal
% solution.

%% Workspace assumption
% Before running this script, the workspace must contain:
%
% * |startIdx| and |endIdx|, the indexes of the starting and the ending
% nodes;
% * |adj|, the adjacency matrix;
% * |values|, the values of each node;
% * |budget|, the budget of the orienteering problem.

%% Workspace definition
% After running this script, the workspace will contain:
%
% * |optPath| the optimal solution;
% * |optCost| the cost of the optimal solution;
% * |optScore| the score of the optimal solution.

%% Run the function
[optPath, optCost, optScore] = opt(startIdx, endIdx, adj, values, budget);