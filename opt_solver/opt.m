function [optPath, optCost, optScore] = opt(s, e, adj, values, budget)
% OPT solve optimally the given orienteering problem.

    % Nodes number of the orienteering problem.
    nNodes = size(adj, 1);
 
    % Initial solution.
    bestPath(1) = s;
    bestPath(nNodes + 1) = e;
    if s ~= e 
        bestScore = values(s) + values(e); % start and end are different points: add their values to the score.
    else
        bestScore = values(s); % start and end are the same point: add only one of their values to the score.
    end
    bestCost = adj(s, e);
    bestNext = 2;
     
    % Try all possible start node's successors.
    for n = 1 : nNodes
        
        % Create a new path that contains only start and end.
        path = zeros(nNodes + 1, 1);
        path(1) = s;
        path(nNodes + 1) = e;
        if s ~= e
            score = values(s) + values(e);
        else
            score = values(s);
        end
        cost = adj(s, e);
        
        % First free position.
        next = 2;
        
        % Boolean array of visited nodes (reduce the complexity of looking if a point is or not in the path to O(1)).
        visited = zeros(nNodes, 1);
        visited(s) = 1;
        visited(e) = 1;
        
        % If the point is already in the path, go to the next.
        if visited(n) == 1
            continue;
        end
            
        % Add the point.
        path(next) = n;
        next = next + 1;
        visited(n) = 1;
        score = score + values(n);
        cost = cost ...
             - adj(path(next - 2), path(nNodes + 1)) ...
             + adj(path(next - 1), path(nNodes + 1)) ...
             + adj(path(next - 2), path(next - 1));
        
        % Check if it is better than the previous best path.
        if score > bestScore || (score == bestScore && cost < bestCost)
            bestPath = path;
            bestNext = next;
            bestScore = score;
            bestCost = cost;
        end
        
        % Recursively visit all neighbours.
        [path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext] = ...
            rec(path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext, adj, values, budget);
        
    end
    
    % Return and set optPath in the right format (i.e., a column array of points).
    optPath = bestPath([1 : bestNext - 1, nNodes + 1]);
    if size(optPath, 1) == 2 
        [uniqueSubPath, idxs] = unique(optPath);
        uniqueSubPath = optPath(sort(idxs));
        optPath = uniqueSubPath;
    end
    optCost = bestCost;
    optScore = bestScore;
    
end

function [path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext] = ...
    rec(path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext, adj, values, budget)
    
    % Try all possible node's successors.
    for n = 1 : nNodes
        
        % If the point is already in the path, go to the next.
        if visited(n) == 1
            continue;
        end
        
        % Try to add the new point.
        path(next) = n;
        next = next + 1;
        visited(n) = 1;
        score = score + values(n);
        cost = cost ...
             - adj(path(next - 2), path(nNodes + 1)) ...
             + adj(path(next - 1), path(nNodes + 1)) ...
             + adj(path(next - 2), path(next - 1));
        
        % Check if the path is feasible and if it is not remove the point and continue.
        if cost > budget
            cost = cost ...
                 + adj(path(next - 2), path(nNodes + 1)) ...
                 - adj(path(next - 1), path(nNodes + 1)) ...
                 - adj(path(next - 2), path(next - 1)); 
            score = score - values(n);
            visited(n) = 0;
            next = next - 1;
            path(next) = 0;
            continue;
        end
        
        % Check if it is better than the previous best path.
        if score > bestScore || (score == bestScore && cost < bestCost)
            bestPath = path;
            bestNext = next;
            bestScore = score;
            bestCost = cost;
        end
        
        % Recursively visit all neighbours.
        [path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext] = ...
            rec(path, cost, score, next, visited, nNodes, bestCost, bestScore, bestPath, bestNext, adj, values, budget);
        
        % Remove the last point.
        cost = cost ...
             + adj(path(next - 2), path(nNodes + 1)) ...
             - adj(path(next - 1), path(nNodes + 1)) ...
             - adj(path(next - 2), path(next - 1)); 
        score = score - values(n);
        visited(n) = 0;
        next = next - 1;
        path(next) = 0;
               
    end        
        
end
