function neighboursIdxs = compute_supernode_neighbours(G, supernodes, H, supernodeIdx, dist)
% COMPUTE_SUPERNODE_NEIGHBOURS compute the neighbours of the given
% supernode index at (Manhattan) distance dist.

    supernode = supernodes{supernodeIdx}; % get the supernode list.
    neighboursIdxs = zeros(1, 4 * dist * size(supernode, 1)); % preallocate the maximum size of neighbours.
    counter = 1;

    % For each node in supernode compute its neighbours.
    for i = 1 : size(supernode, 1)
        node_neighbours = compute_node_neighbours(G, H, supernodeIdx, supernode(i, 1), supernode(i, 2), dist);
        neighboursIdxs(counter : counter + size(node_neighbours, 2) - 1) = node_neighbours;
        counter = counter + size(node_neighbours, 2);
    end

    % Clean the list of neighbours.
    neighboursIdxs = neighboursIdxs(1 : counter - 1);
    neighboursIdxs = unique(neighboursIdxs);

end


function neighboursIdxs = compute_node_neighbours(G, H, supernodeIdx, i, j, dist)

    neighboursIdxs = [];

    % Compute neighbours on each side.
    neighboursIdxs = [neighboursIdxs, compute_node_neighbours_aux(G, H, supernodeIdx, i - dist, j, i, j + dist, dist)];
    neighboursIdxs = [neighboursIdxs, compute_node_neighbours_aux(G, H, supernodeIdx, i, j + dist, i + dist, j, dist)];
    neighboursIdxs = [neighboursIdxs, compute_node_neighbours_aux(G, H, supernodeIdx, i + dist, j, i, j - dist, dist)];
    neighboursIdxs = [neighboursIdxs, compute_node_neighbours_aux(G, H, supernodeIdx, i, j - dist, i - dist, j, dist)];

    neighboursIdxs = unique(neighboursIdxs);

end


function neighbours = compute_node_neighbours_aux(G, H, supernodeIdx, si, sj, ei, ej, dist)

    % Compute the correct indexes increment.
    if (si > ei); iInc = -1; else; iInc = 1; end;
    if (sj > ej); jInc = -1; else; jInc = 1; end;
    
    neighbours = zeros(1, dist); % preallocate the maximum neighbours size.
    counter = 1;
    
    i = si;
    j = sj;
    
    % Compute valid neighbours.
    while i ~= ei && j ~= ej
        if i > 0 && i <= size(G, 1) && j > 0 && j <= size(G, 2) && G(i, j) ~= 0
            if H(i, j) ~= supernodeIdx
                neighbours(counter) = H(i, j);
                counter = counter + 1;
            end
        end
        i = i + iInc;
        j = j + jInc;
    end
    
    % Truncate the unused cols.
    neighbours = neighbours(1 : counter - 1); 
    
end
