function [G, supernodes, supernodesSizes, H] = init_supernodes(G, si, sj, ei, ej, budget)
% INIT_SUPERNODES initialize the structures according to the orienteering
% instance.
    
    [r, c] = size(G);
        
    % Preallocate structures.
    H = zeros(r, c); % keep track of the supernode of each node in G.
    supernodes = cell(r * c, 1); 
    supernodesSizes = zeros(r * c, 1);
    
    counter = 1; 

    for i = 1 : r
        for j = 1 : c
            % Add a new non zero node ...
            if G(i, j) ~= 0 || (si == i && sj == j) || (ei == i && ej == j)
                % ... only if it is in the ellipse, otherwise set it to 0.
                if dst([si sj], [i j]) + dst([ei ej], [i j]) > budget
                    G(i, j) = 0;
                    continue;
                end;
                supernodes{counter} = [i, j];
                supernodesSizes(counter) = 1;
                H(i, j) = counter;
                
                counter = counter + 1;
            end
        end
    end

    % Truncate the unused rows.
    supernodes = supernodes(1 : counter - 1, :); 
    supernodesSizes = supernodesSizes(1 : counter - 1);
end
