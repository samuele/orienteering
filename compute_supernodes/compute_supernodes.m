%% Compute the nodes of the reduced graph
% The reduced graph (i.e., the supergraph) is a smaller graph in which
% every node is made up from a set of (close together) points of the
% orienteering problem.

%% Workspace assumption
% Before running this script, the workspace must contain:
%
% * |G| the orienteering problem world;
% * |[si, sj]| and |[ei, ej]| the starting point and the ending point of
% the problem;
% * |budget| the maximum budget of the orienteering problem.

%% Workspace definition
% After running this script, the workspace will contain:
%
% * |K|;
% * |supernodes|;
% * |supernodesSizes|;
% * |H|;
% * |nSupernodes|.
%
% The content of these variables is explained in the following sections.

%% Maximum number of nodes in the supergraph (i.e., supernodes)
% If the orienteering problem nodes are less than or equal to |K|, the
% supergraph's nodes match the original ones, otherwise a reduced graph
% with |K| supernodes will be created. This parameter should be adjusted
% according to the speed of the optimal solver.
K = optimalSolverCardinality;

%% Initialization
% Get the initial value of the following structures:
%
% * |supernodes| is a cell column array such that |supernodes(i)| is the
% i-th node of the reduced graph that contains a |Lx2| double matrix which
% represents a list of nodes coordinates. Each cell is a valid or not valid
% supernode (see |supernodesSizes|);
%
% * |supernodesSizes| is a double column array of the same size of
% |supernodes|. The i-th element in |superodesSizes| is the size of the
% i-th supernode in |supernodes|. If the size is |Inf|, the i-th supernode
% in |supernodes| is not valid;
%
% * |H| is a double matrix of the same size of |G|. |H(i, j)| is the index
% of the supernode which |G(i, j)| belongs to.
[G, supernodes, supernodesSizes, H] = init_supernodes(G, si, sj, ei, ej, budget);

%%
% Number of node in the supergraph.
nSupernodes = size(supernodes, 1);
 
%% Algorithm
% Keep merging supernodes until they are equal to K.
while nSupernodes > K
        
    % Compute a random smallest supernode.
    smallestSupernodesIdxs = find(supernodesSizes == min(supernodesSizes));
    %rng(12); %%% DEBUG %%%
    randIdx = randperm(size(smallestSupernodesIdxs, 1), 1);
    randSupernodeIdx = smallestSupernodesIdxs(randIdx);
    
    % Compute the random node's closest neighbours using the Manhattan distance. 
    neighboursIdxs = [];
    dist = 1;
    while isempty(neighboursIdxs)
        neighboursIdxs = compute_supernode_neighbours(G, supernodes, H, randSupernodeIdx, dist);
        dist = dist + 1;
    end
    
    % Compute a random smallest neighbour.
    neighboursSizes = supernodesSizes(neighboursIdxs);
    smallestNeighboursIdxs = neighboursIdxs(transpose(find(neighboursSizes == min(neighboursSizes))));
    %rng(12); %%% DEBUG %%%
    randSmallestNeighbourIdx = randperm(size(smallestNeighboursIdxs, 2), 1);
        
    % Merge the two supernodes.
    [supernodes, supernodesSizes, H] = ...
        merge(supernodes, supernodesSizes, H, randSupernodeIdx, smallestNeighboursIdxs(randSmallestNeighbourIdx));
    nSupernodes = nSupernodes - 1;
end

%% Clear workspace
% Remove useless definitions.
clear('smallestSupernodesIdxs', 'randIdx', 'randSupernodeIdx', 'neighboursIdxs', 'dist', 'neighboursSizes', ...
    'smallestNeighboursIdxs', 'randSmallestNeighbourIdx');
