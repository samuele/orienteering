function [supernodes, supernodesSizes, H] = merge(supernodes, supernodesSizes, H, x, y)
% MERGE merge the supernode with index y with the supernode with index x.
% After the merging, the y supernode is set to not valid.

    % Update the supernodes table.
    list = supernodes{y};
    for i = 1 : size(list, 1)
        H(list(i, 1), list(i, 2)) = x;
    end
    
    % Merge and delete.
    supernodes{x} = [supernodes{x}; supernodes{y}];
    supernodesSizes(x) = supernodesSizes(x) + supernodesSizes(y);
    supernodesSizes(y) = Inf;

end
