function [bestRoute, bestRouteValue, nodesMatrix, pathOP] = solveOrienteering(matrixATX, startNode, endNode, budget, unitOfDistance)
    
    % Compute the enumeration matrix.
    nodesMatrix = zeros(size(matrixATX));
    cnt = 1;
    for i = 1 : size(matrixATX, 1)
        for j = 1 : size(matrixATX, 2)
            if matrixATX(i, j) == 0
                continue
            end
            nodesMatrix(i, j) = cnt;
            cnt = cnt + 1;
        end
    end
    
    % Define all the variable names expected by start_script.
    G = matrixATX;
    si = startNode(1, 1);
    sj = startNode(1, 2);
    ei = endNode(1, 1);
    ej = endNode(1, 2);
    budget = budget / unitOfDistance;
    
    % Solve the given orienteering problem.
    start_script
    
    % Output the route as an enumerated list of nodes.
    bestRoute = transpose(nodesMatrix(sub2ind(size(nodesMatrix), pathOP.p{1, 1}(:, 1), pathOP.p{1, 1}(:, 2))));
    bestRouteValue = pathOP.p{1, 3};
    
end

