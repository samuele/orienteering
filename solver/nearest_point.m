function [i, j] = nearest_point(A, p)
% NEAREST_POINT return the first point of A nearest to point p.

    r = size(A, 1);
    minDst = realmax('double');
    i = 0;
    j = 0;
    
    for m = 1 : r
        q = A(m, :);
        qr = q - 0.5;
        newDst = dst(p, qr);
        if newDst < minDst
            i = q(1);
            j = q(2);
            minDst = newDst;
        end
    end
        
end
