function [globalPath, globalCost, globalScore] = solver(G, si, sj, ei, ej, budget)
% SOLVER solve the given orienteering problem.

    % Initialization.
    globalPath = [si, sj; ei, ej];
    if si == ei && sj == ej
        globalScore = G(si, sj);
    else
        globalScore = G(si, sj) + G(ei, ej);
    end
    globalCost = dst([si, sj], [ei, ej]);
    
    compute_supernodes;
    build_supergraph;
    
    % Empty graph case.
    if nSupernodes == 0
        return
    end
    
    opt_solver;
        
    % Base case.
    if size(supernodes, 1) <= K
        globalPath = cell2mat(normSupernodes(transpose(optPath)));
        globalScore = optScore;
        globalCost = optCost;
        return;
    end

    % Sort the subproblems: we want to solve first the problems with less nodes.
    map = horzcat(optPath, normSupernodesSizes(optPath));
    [V, I] = sort(map(:, 2));
    orderedMap = map(I, :);
    points = orderedMap(:, 1);
    
    % Reinitialization.
    globalPath = cell(size(orderedMap, 1), 1);
    globalScore = 0;
    globalCost = 0;
    
    % Recursive solve the subproblems.
    remainingBudget = 0;
    [uniquePoints, idxs] = unique(points); % can remove the last point if the start and end are the same point. 
    uniquePoints = points(sort(idxs));

    for l = 1 : size(uniquePoints, 1)
        i = uniquePoints(l);
        list = normSupernodes{i};

        % Create the new graph.
        newG = zeros(size(G));
        for j = 1 : size(list, 1)
            coord = list(j, :);
            newG(coord(1), coord(2)) = G(coord(1), coord(2));
        end

        % Set the new budget.
        newBudget = budget / size(uniquePoints, 1) + remainingBudget;
    
        % Compute the new start and the new end.
        % (Don't ask how).
        if i == map(1, 1)
            newS = [si, sj];
        elseif isempty(globalPath{find(map(:, 1) == i) - 1})
            [r, c] = nearest_point(list, centersOfMass(map(find(map(:, 1) == i) - 1, 1), :));
            newS = [r, c];
        else
            prevPath = globalPath{find(map(:, 1) == i) - 1};
            prevEnd = prevPath(end, :);
            [r, c] = nearest_point(list, prevEnd - 0.5);
            newS = [r, c];
        end
       
        if i == map(end, 1) && i ~= map(1, 1)
            newE = [ei, ej];
        elseif isempty(globalPath{find(map(:, 1) == i, 1 ) + 1}) % find first.
            [r, c] = nearest_point(list, centersOfMass(map(find(map(:, 1) == i, 1 ) + 1, 1), :)); % find first.
            newE = [r, c];
        else
            succPath = globalPath{find(map(:, 1) == i) + 1};
            succStart = succPath(1, :);
            [r, c] = nearest_point(list, succStart - 0.5);
            newE = [r, c];
        end
        
        % Recursion.
        [p, c, s] = solver(newG, newS(1), newS(2), newE(1), newE(2), newBudget);
        
        % Remove the last point if it is the same of the initial point.
        if size(p, 1) > 1 && isequal(p(1, :), p(end, :))
            secondToLast = p(end - 1, :);
            last = p(end, :);
            c = c - dst(secondToLast, last); % reduce the cost.
            p(end, :) = []; % remove the point.             
        end
            
        % Update global info.
        globalCost = globalCost + c;
        globalScore = globalScore + s;
        pos = find(map(:, 1) == i, 1); % find first.
        globalPath{pos} = p;
        remainingBudget = newBudget - c;
        
        % Add the cost from previous last point to this first point.
        if pos - 1 > 0 && ~isempty(globalPath{pos - 1})
            previousPath = globalPath{pos - 1};
            lastPreviousPathPoint = previousPath(end, :);
            firstPoint = p(1, :);
            globalCost = globalCost + dst(lastPreviousPathPoint, firstPoint); 
        end
    
        % Add the cost from this last point to the next first point.
        if pos + 1 <= size(globalPath, 1) && ~isempty(globalPath{pos + 1})
            succPath = globalPath{pos + 1};
            firstSuccPathPoint = succPath(1, :);
            lastPoint = p(end, :);
            globalCost = globalCost + dst(firstSuccPathPoint, lastPoint);
        end
    end
        
    % If the starting point is the same of the ending point, insert the ending point.
    if size(points, 1) ~= size(uniquePoints, 1)
        globalPath{end} = [si, sj];
        lastPath = globalPath{end - 1};
        lastPoint = lastPath(end, :);
        globalCost = globalCost + dst(lastPoint, [si, sj]);
    end
    
    globalPath = cell2mat(globalPath);
    
    [globalPath, globalCost, globalScore] = checkCost(globalPath, globalCost, globalScore, G, budget);

end

function [globalPath, globalCost, globalScore] = checkCost(globalPath, globalCost, globalScore, G, budget)

    % Base case.
    if globalCost < budget
        return
    end
    
    % Initialization.
    pathLen = size(globalPath, 1);
    min = intmax;
    mina = 0;
    minb = 0;
    minc = 0;
    idx = 0;
    
    % Find the worst point.
    for i = 2 : pathLen - 1
        a = dst(globalPath(i - 1, :), globalPath(i, :));
        b = dst(globalPath(i + 1, :), globalPath(i, :));
        c = dst(globalPath(i - 1, :), globalPath(i + 1, :));
        p = (a + b + c) / 2;
        h = 2 * sqrt(p * (p - a) * (p - b) * (p - c)) / c;
        if h > 0
            val = G(globalPath(i, 1), globalPath(i, 2)) / h;
        else
            continue
        end
        
        if val < min
            min = val;
            mina = a;
            minb = b;
            minc = c;
            idx = i;
        end
    end
    
    % If idx is zero (the path is straightforward and come back to the
    % start), remove the point with largest distance to the start.
    max = 0;
    if idx == 0
        for i = 2 : pathLen - 1
            if dst(globalPath(1, :), globalPath(i, :)) > max 
                max = dst(globalPath(1, :), globalPath(i, :));
                idx = i;
            end
        end
        
        globalScore = globalScore - G(globalPath(idx, 1), globalPath(idx, 2));
        globalCost = globalCost - (dst(globalPath(i - 1, :), globalPath(i, :)) + dst(globalPath(i + 1, :), globalPath(i, :)));
        globalPath(idx, :) = [];
        
        % Recursive step.
        [globalPath, globalCost, globalScore] = checkCost(globalPath, globalCost, globalScore, G, budget);
        return
    end
            
    
    % Remove it and update the structure.
    globalScore = globalScore - G(globalPath(idx, 1), globalPath(idx, 2));
    globalCost = globalCost - (mina + minb) + minc;
    globalPath(idx, :) = [];
    
    % Recursive step.
    [globalPath, globalCost, globalScore] = checkCost(globalPath, globalCost, globalScore, G, budget);
end
