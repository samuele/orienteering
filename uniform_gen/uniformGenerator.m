function [matrixATX, startNode, endNode, budget, unitOfDistance] = uniformGenerator(r, c)
    
    matrixATX = 0 + (127.5 + 127.5)*rand(r, c);
    
    si = randi([1 r], 1, 1);
    sj = randi([1 c], 1, 1);
    ei = randi([1 r], 1, 1);
    ej = randi([1 c], 1, 1);

    startNode = [si sj];
    endNode = [ei ej];
    
    unitOfDistance = 1;
    
    budget = r * c * 0.5;
end
