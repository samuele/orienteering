function gen(r, c, howmany)
    
    for i = 1 : howmany
        [matrixATX, startNode, endNode, budget, unitOfDistance] = uniformGenerator(r, c);
        save(['setup_', num2str(i)], 'matrixATX', 'startNode', 'endNode', 'budget', 'unitOfDistance');
    end

end

