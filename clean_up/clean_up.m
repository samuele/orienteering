function [moves, improvement] = clean_up(pathOP, G, moves, improvement)
    
    % 2-opt.
    
    [R, C] = size(G);
    currentPath = pathOP.p;
    bestCost = pathOP.p{1, 2};
    [adj, mapObj] = computeStructures(pathOP.p{1, 1}, R, C);
    
    toALoop = true;
    imp = false;

    while toALoop
        toALoop = false;
   
        for j = 2 : size(currentPath{1, 1}, 1) - 2
       
            if toALoop
                break
            end
       
            for k = j + 1 : size(currentPath{1, 1}, 1) - 1
           
                newPath = twoOptSwap(currentPath, j, k, adj, mapObj);

                if newPath{1, 2} < bestCost
                    currentPath = newPath;
                    bestCost = newPath{1, 2};
                    toALoop = true;
                    imp = true;
                    moves = true;
                    improvement = true;
                    break
                end
           
            end
       
        end
   
    end

    if imp
        pathOP.p = currentPath;
    end
end

function [res] =  twoOptSwap(currentPath, j, k, adj, mapObj)

    res = cell(1, 3);
    res{1, 1} = zeros(size(currentPath{1,1}, 1), 2);
    res{1, 2} = 0;
    res{1, 3} = currentPath{1, 3};
    
    res{1, 1}(1, :) = currentPath{1, 1}(1, :);
    secondToLastInsertedIdx = 1;
    
    for i = 2 : size(res{1, 1}, 1)
       
        if j <= i && i <= k
            res{1, 1}(i, :) = currentPath{1, 1}(k - i + j, :);
        else
            res{1, 1}(i, :) = currentPath{1, 1}(i, :);
        end
        
        lastInsertedIdx = mapObj(res{1, 1}(i, 1), res{1, 1}(i, 2));
        
        res{1, 2} = res{1, 2} + adj(lastInsertedIdx, secondToLastInsertedIdx);
        
        secondToLastInsertedIdx = lastInsertedIdx;
    end
    
end

function [adjMatrix, mapObj] = computeStructures(p, R, C)

    dim = size(p, 1);
    adjMatrix = zeros(dim, dim);
    
    mapObj = zeros(R, C);
    
    for i = 1 : dim
        for j = i : dim
            if i == j
                continue
            end
            distance = dst(p(i, :), p(j, :));
            adjMatrix(i, j) = distance;
            adjMatrix(j, i) = distance;
        end
        
        mapObj(p(i, 1), p(i, 2)) = i;
    end
end
