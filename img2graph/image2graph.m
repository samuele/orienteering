function [matrixATX, startNode, endNode, budget, unitOfDistance] = image2graph(path2image, densityOfZeros, maxDim)
    
    matrixATX = imread(path2image);
    
    maxsize = max(size(matrixATX));
    scaleFact = 100 * maxDim / maxsize;
    
    matrixATX = imresize(matrixATX, scaleFact / 100);
    
    matrixATX = matrixATX(:, :, 1);
    matrixATX = double(matrixATX);
    matrixATX = matrixATX * 0.1;
    V = matrixATX(:);
    V = sort(V);
    
    maxsize = max(size(V));
    n = floor(maxsize * densityOfZeros);
    V = V(1 : n);
    
    for i = 1 : n
        [row, col] = find(matrixATX == V(i), 1);
        matrixATX(row, col) = 0;
    end
    
    [r, c] = size(matrixATX);
    
    si = randi([1 r], 1, 1);
    sj = randi([1 c], 1, 1);
    ei = randi([1 r], 1, 1);
    ej = randi([1 c], 1, 1);

    startNode = [si sj];
    endNode = [ei ej];
    
    unitOfDistance = 1;
    
    budget = nnz(matrixATX);
end
