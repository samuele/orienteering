function dir2setup(folder)
    
    if folder(end) ~= '\' && folder(end) ~= '/'
        folder = [folder, '/'];
        
    files = dir([folder, '*.jpg']);
    i = 1;
    for file = files'
        [matrixATX, startNode, endNode, budget, unitOfDistance] = image2graph([file.folder, '/', file.name], 0, 12);
        save(['setup_', num2str(i)], 'matrixATX', 'startNode', 'endNode', 'budget', 'unitOfDistance');
        i = i + 1;
    end

end

