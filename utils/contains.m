function [b] = contains(node, path)

    if isempty(node) || isempty(path{1,1})
        b = false;
        return
    end

     
    if isempty(find(path{1,1}(:, 1) == node(1) & path{1,1}(:, 2) == node(2), 1))
        b = false;
    else
        b = true;
    end
    
end