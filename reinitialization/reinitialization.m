function reinitialization(pathOP, G, k)

    % Removes the k nodes that have the smallest ratio.
   
    opLen = size(pathOP.p{1, 1}, 1);
    ratioVector = zeros(opLen - 2, 1);
    
    for i = 2 : opLen - 1
        ratioVector(i - 1, 1) = G(pathOP.p{1, 1}(i, 1), pathOP.p{1, 1}(i, 2)) ...
                             / (dst(pathOP.p{1, 1}(i, :), pathOP.p{1, 1}(i - 1, :)) ...
                              + dst(pathOP.p{1, 1}(i, :), pathOP.p{1, 1}(i + 1, :)));
    end

    for i = 1 : k
        if isempty(ratioVector)
            break
        end
        
        idxMin = find(ratioVector(:, 1) == min(ratioVector));
        
        if size(idxMin, 1) > 1
            idxMin = idxMin(1);
        end
        
        ratioVector(idxMin) = [];
        
        pathOP.p{1, 3} = pathOP.p{1, 3} - G(pathOP.p{1, 1}(idxMin + 1, 1), pathOP.p{1, 1}(idxMin + 1, 2));
        
        pathOP.p{1, 2} = pathOP.p{1, 2} - dst(pathOP.p{1, 1}(idxMin + 1, :), pathOP.p{1, 1}(idxMin, :)) ...
                                    - dst(pathOP.p{1, 1}(idxMin + 1, :), pathOP.p{1, 1}(idxMin + 2, :)) ...
                                    + dst(pathOP.p{1, 1}(idxMin, :), pathOP.p{1, 1}(idxMin + 2, :));
        
        pathOP.p{1, 1}(idxMin + 1, :) = [];
    end
    
end
