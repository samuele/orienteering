function draw_path(pathOP, alg_name)

    y = pathOP.p{1, 1}(:, 1);
    x = pathOP.p{1, 1}(:, 2);
    
    plot(x, y, 'DisplayName', [alg_name ': [score, cost] = [' num2str(pathOP.p{1, 3}) ', ' num2str(pathOP.p{1, 2}) ']']);
    legend('-DynamicLegend', 'Location', 'northeastoutside')
  
end
