x0 = 10;
y0 = 10;
width = 1024;
height = 768;
set(gcf, 'units', 'points', 'position', [x0, y0, width, height]);

imagesc(matrixATX);
colormap(gray);
hold all

if exist('orienteeringInstanceTitle')
    title(orienteeringInstanceTitle);
end
