function save_and_close(path_to_file)

    saveas(gcf, path_to_file);
    hold off
    close(gcf);    

end

