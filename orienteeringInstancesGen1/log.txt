function [K] = nImprovementRuns()
    K = 5;
end
function [K] = nInitializationRuns()
    K = 10;
end
function [K] = nPathsNOP()
    K = 4;
end
function [K] = nReinitializationRuns()
    K = 5;
end
function [K] = nRuns()
    K = 10;
end
function [K] = optimalSolverCardinality()
    K = 10;
end
function [K] = reinitializationBound()
    K = 0.1;
end
Starting MATLAB engine and configure the environment... Done.
0%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
9%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
18%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
27%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
36%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
45%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
55%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
64%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
73%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
82%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
91%
Loading environment... Done.
Drawing the model... Done.
Running fastOP algorithm... Done.
Drawing path on the model... Done.
Running New algorithm... Done.
Drawing path on the model... Done.
Saving figure and closing... Done.
100%

Stats generation...
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+       Instance       ||   Points    ||       Score       ||       Cost        ||       Time        || Improvement (Score) || Improvement (Cost) +
+-------------------------------------------------------------------------------------------------------------------------------------------------+
+         setup_11.mat ||  111    115 || 2596.60   2705.70 ||  161.74    161.60 ||   52.28   1786.92 ||        +4.20%       ||        -0.09%      +
+          setup_3.mat ||  116     93 ||  640.90    572.50 ||  161.76    161.94 ||  131.70   1012.15 ||       -10.67%       ||        +0.11%      +
+          setup_1.mat ||  105    103 || 2043.60   2026.90 ||  161.93    161.51 ||   36.52   1369.40 ||        -0.82%       ||        -0.26%      +
+          setup_5.mat ||  102     98 ||  807.20    875.20 ||  160.90    161.67 ||   69.04    916.94 ||        +8.42%       ||        +0.48%      +
+          setup_2.mat ||  106    103 || 1636.50   1694.80 ||  161.99    161.67 ||   63.04   1485.27 ||        +3.56%       ||        -0.20%      +
+         setup_10.mat ||  108    109 ||  556.80    590.30 ||  161.92    161.54 ||   52.47   1958.67 ||        +6.02%       ||        -0.24%      +
+          setup_7.mat ||   80     66 || 1028.20    925.60 ||  161.86    161.99 ||   45.42    536.99 ||        -9.98%       ||        +0.08%      +
+          setup_9.mat ||  118    122 || 2222.80   2354.30 ||  161.71    161.45 ||  180.04   2084.76 ||        +5.92%       ||        -0.16%      +
+          setup_6.mat ||  107    109 || 2176.10   2242.90 ||  161.98    161.57 ||   99.20   1395.35 ||        +3.07%       ||        -0.26%      +
+          setup_4.mat ||   81     91 || 1882.10   2139.60 ||  161.83    161.45 ||   54.97    750.41 ||       +13.68%       ||        -0.24%      +
+          setup_8.mat ||   96    106 || 2397.00   2677.50 ||  161.96    161.81 ||   30.04   1037.54 ||       +11.70%       ||        -0.09%      +
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Final results generation...
+-------------------------------------+
+ Avg. score improvement       +3.19% +
+ Avg. cost improvement        -0.08% +
+ Avg. execution time        1303.13s +
+-------------------------------------+
