function [pathOP, pathsNOP] = initialization(G, si, sj, ei, ej, budget)
    
    % Initialization.
    maxValue = 0;
    cost = intmax;
    maxIdx = 1;

    % Build pathOP and pathsNOP.
    for i = 1 : nPathsNOP + 1
        % Solve the initialization phase on a reduced graph.
        solver_script;
        
        % Save the result.
        pathsNOP(i) = path;
        pathsNOP(i).p{1, 1} = globalPath;
        pathsNOP(i).p{1, 2} = globalCost;
        pathsNOP(i).p{1, 3} = globalScore;
   
        % Keep track of the best result.
        if globalScore > maxValue || (globalScore == maxValue && globalCost < cost) 
            maxValue = globalScore;
            maxIdx = i;
        end
    end

    % Set pathOP as the best generated path in pathNOP.
    pathOP = path;
    pathOP.p = pathsNOP(maxIdx).p;
    pathsNOP(maxIdx) = [];

end

