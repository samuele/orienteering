% Initialization.
N = nInitializationRuns;
NP = nPathsNOP;
next = 1;
values = zeros(1, N * NP + 1);

% Run initialization N times.
for i = 1 : N
    %disp(['N = ' num2str(i)]) %%% DEBUG %%%
    [pathOP, pathsNOP] = initialization(G, si, sj, ei, ej, budget);
    
    % Copy pathOP in paths.
    paths(next) = path;
    paths(next).p = pathOP.p;
    values(next) = pathOP.p{1, 3};
    next = next + 1;
    
    % Copy pathsNOP in paths.
    for j = 1 : NP
        paths(next) = path;
        paths(next).p = pathsNOP(1, j).p;
        values(next) = pathsNOP(1, j).p{1, 3};
        next = next + 1;
    end
end

% Sort and keep the first NP + 1 paths.
[sortedValues, indexes] = sort(values, 'descend');
pathOP = paths(indexes(1));
pathsNOP(1, 1 : NP) = paths(indexes(2 : NP + 1)); 

clear('N', 'next', 'values', 'i', 'paths', 'j', 'indexes', 'sortedValues');