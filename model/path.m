classdef path < handle
    
    properties
        p
    end
    
    methods
        
       function initPath(obj, route, cost, score)
            obj.p = cell(1, 3);
            obj.p{1, 1} = route;
            obj.p{1, 2} = cost;
            obj.p{1, 3} = score;
       end
        
       function [score] = score(obj)
           score = obj.p{1, 3};
       end
       
       function [cost] = cost(obj)
           cost = obj.p{1, 2};
       end
       
       function [len] = len(obj)
           len = size(obj.p{1, 1}, 1);
       end
    end
    
end
