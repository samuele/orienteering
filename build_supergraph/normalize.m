function [normSupernodes, normSupernodesSizes, normH] = normalize(supernodes, supernodesSizes, H)
% NORMALIZE remove all not valid supernodes, i.e., all the supernodes whose
% size is set to Inf.

    validIdxs = find(supernodesSizes ~= Inf);
    
    normSupernodes = supernodes(validIdxs);
    normSupernodesSizes = supernodesSizes(validIdxs);
    normH = H;
    
    for i = 1 : size(normSupernodesSizes, 1)
        list = normSupernodes{i};
        for j = 1 : size(list, 1)
            normH(list(j, 1), list(j, 2)) = i;
        end
    end
 
end

