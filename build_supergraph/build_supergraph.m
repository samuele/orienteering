%% Build the supergraph from the set of supernodes
% The result of the execution of this script is a new adjacency matrix
% based on the supernodes' center of mass. In addition, it set the new
% starting and ending points.

%% Workspace assumption
% Before running this script, the workspace must contain:
%
% * |G|;
% * |[si, sj]| and |[ei, ej]|;
% * |supernodes|;
% * |supernodesSizes|;
% * |H|.
%
% All these variables are described in |compute_supernodes.m|.

%% Workspace definition
% After running this script, the workspace will contain:
%
% * |normSupernodes|;
% * |normSupernodesSizes|;
% * |normH|;
% * |nNormSupernodes|;
% * |centersOfMass|;
% * |values|;
% * |adj|;
% * |startIdx| and |endIdx|;
%
% The content of these variables is explained in the following sections.

%% Remove not valid supernodes
% Remove all supernodes whose size is set to |Inf| and adjust |H| according
% to the new information.
[normSupernodes, normSupernodesSizes, normH] = normalize(supernodes, supernodesSizes, H);

%%
% The number of the valid supernodes.
nNormSupernodes = size(normSupernodes, 1);

%% Preallocate all the variables

% centersOfMass(i, :) will be the coordinates of the i-th supernode's
% center of mass.
centersOfMass = zeros(nNormSupernodes, 2);

% values(i) will be the value of the i-th supernode (the average of the
% values of its nodes.
values = zeros(nNormSupernodes, 1);

% The new adjacency matrix.
adj = zeros(nNormSupernodes, nNormSupernodes);

%% Compute supernodes' values and centers of mass
% For each valid supernodes...
for i = 1 : nNormSupernodes
    
    % ... compute center of mass and value.
    [centersOfMass(i, 1), centersOfMass(i, 2), values(i)] = ...
        compute_supernodes_info(G, normSupernodes{i}, normSupernodesSizes(i));
    
    % Update the adjacency matrix.
    for j = 1 : i
        adj(j, i) = dst(centersOfMass(i, :), centersOfMass(j, :));
        adj(i, j) = adj(j, i);
    end
end

%% Set the initial and ending supernodes
% The new starting and ending nodes are the supernodes which contains the
% original ones.
startIdx = normH(si, sj);
endIdx = normH(ei, ej);

%% Clear workspace
% Remove useless definitions.
clear('i', 'j');