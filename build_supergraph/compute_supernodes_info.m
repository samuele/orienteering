function [i, j, value] = compute_supernodes_info(G, list, len)
% COMPUTE_SUPERNODES_INFO compute the center of mass of the given
% supernodes and its new value.

    numI = 0;
    numJ = 0;
    den = 0;
    
    for l = 1 : len
        v = G(list(l, 1), list(l, 2));
        numI = numI + ((list(l, 1) - 0.5) * v);
        numJ = numJ + ((list(l, 2) - 0.5) * v);
        den = den + v;
    end
    
    % If den is zero, it means that all nodes values are zero: in these
    % case, compute the center of gravity instead of the center of mass.
    if den ~= 0
        i = numI / den;
        j = numJ / den;
    else
        numI = 0;
        numJ = 0;
        for l = 1 : len
            numI = numI + (list(l, 1) - 0.5);
            numJ = numJ + (list(l, 2) - 0.5);
        end
        
        i = numI / len;
        j = numJ / len;
    end
    
    value = den;
    
end
