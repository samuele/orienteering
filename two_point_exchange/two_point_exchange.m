function [moves, improvement] = two_point_exchange(pathOP, pathsNOP, G, budget, moves, improvement, record, deviation)

    % For j = the first to the last point in pathOP (A LOOP)
    %     For i = the first to the last point in the first to the last path in pathNOP (B LOOP)
    %         If exchanging i and j is feasible and the total score increases, do the exchange and go to the A loop
    %         Else Set the best exchange = one with the highest score
    %     End B loop
    %     If the score of the best exchange > record - deviation, make the best exchange
    % End A loop
    
    sizeOP = size(pathOP.p{1, 1}, 1);
    sizeNOP = size(pathsNOP);
    
    breakDueToImprovement = false;
    bestWorstScore = 0;
    
    node1 = [];
    node2 = [];                     
    path1 = path;
    path1.p = cell(1, 3);
    path2 = path;
    path2.p = cell(1, 3);
    
    v = 2 : sizeOP - 1;
    shuffledPathOPPoints = v(randperm(length(v))); 
    for i = shuffledPathOPPoints
        
        nodeOP = pathOP.p{1, 1}(i, :);
        
        v = 1 : sizeNOP;
        shuffledPathsNOP = v(randperm(length(v)));
        for j = shuffledPathsNOP
            
           if breakDueToImprovement 
               breakDueToImprovement = false;
               break;
           end
            
            pathj = pathsNOP(j);
            sizePathj = size(pathj.p{1, 1}, 1);
            
            v = 2 : sizePathj - 1;
            shuffledPathsNOPPoints = v(randperm(length(v)));
            for p = shuffledPathsNOPPoints
                
                nodeNOP = pathj.p{1, 1}(p, :);
                
                oldScore = pathOP.p{1, 3};
                
                if contains(nodeNOP, pathOP.p) || ~contains(nodeOP, pathOP.p) || ~contains(nodeNOP, pathj.p)
                    continue;
                end
                
                exchange(i, pathOP, p, pathj, G);
                pathsNOP(j) = pathj;
                
                if pathOP.p{1, 2} > budget
                    exchange(i, pathOP, p, pathj, G);
                    pathsNOP(j) = pathj;
                    continue;
                end
                
                if pathOP.p{1, 3} > oldScore
                    improvement = true;
                    moves = true;
                    breakDueToImprovement = true;
                    bestWorstScore = 0;
                else
                    newScore = pathOP.p{1, 3};
                    exchange(i, pathOP, p, pathj, G);
                    pathsNOP(j) = pathj;
                    
                    if newScore > bestWorstScore
                        bestWorstScore = newScore;
                        idxOP = i;
                        idxNOP = p;
                        node1 = nodeOP;
                        node2 = nodeNOP;                     
                        path1.p = pathOP.p;
                        path2.p = pathj.p;
                    end
                end
            end
        end
        
       if ~isempty(path1.p{1,1}) && (contains(node2, path1.p) || ~contains(node1, path1.p) || ~contains(node2, path2.p))
            continue;
        end
        
        if bestWorstScore >= record - deviation
            exchange(idxOP, path1, idxNOP, path2, G);
            pathsNOP(j) = pathj;
            moves = true;
            
            node1 = [];
            node2 = [];
        
        end
    end
      
end


function exchange(idx1, path1, idx2, path2, G)

    % Exchange points.
    tmp = path1.p{1, 1}(idx1, :);
    path1.p{1, 1}(idx1, :) = path2.p{1, 1}(idx2, :);
    path2.p{1, 1}(idx2, :) = tmp;
    
    % Update the cost.
    path1.p{1, 2} = path1.p{1, 2} ... 
                  - dst(path2.p{1, 1}(idx2, :), path1.p{1, 1}(idx1 - 1, :)) - dst(path2.p{1, 1}(idx2, :), path1.p{1, 1}(idx1 + 1, :)) ...
                  + dst(path1.p{1, 1}(idx1, :), path1.p{1, 1}(idx1 - 1, :)) + dst(path1.p{1, 1}(idx1, :), path1.p{1, 1}(idx1 + 1, :));
             
    path2.p{1, 2} = path2.p{1, 2} ... 
                  - dst(path1.p{1, 1}(idx1, :), path2.p{1, 1}(idx2 - 1, :)) - dst(path1.p{1, 1}(idx1, :), path2.p{1, 1}(idx2 + 1, :)) ...
                  + dst(path2.p{1, 1}(idx2, :), path2.p{1, 1}(idx2 - 1, :)) + dst(path2.p{1, 1}(idx2, :), path2.p{1, 1}(idx2 + 1, :));
    
    % Update the score.
    path1.p{1, 3} = path1.p{1, 3} - G(path2.p{1, 1}(idx2, 1),  path2.p{1, 1}(idx2, 2)) + G(path1.p{1, 1}(idx1, 1),  path1.p{1, 1}(idx1, 2));
    path2.p{1, 3} = path2.p{1, 3} - G(path1.p{1, 1}(idx1, 1),  path1.p{1, 1}(idx1, 2)) + G(path2.p{1, 1}(idx2, 1),  path2.p{1, 1}(idx2, 2));

end
