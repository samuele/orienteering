% Initialization.
bestPath = path;
bestPath.p{1, 3} = -1;

for i = 1 : nRuns
    %disp(['run = ' num2str(i)]) %%% DEBUG %%%
    initialization_script
    improvement_script
    if pathOP.p{1, 3} > bestPath.p{1, 3}
        bestPath.p = pathOP.p;
    end
end

% Keep pathOP in the environment and clear other variables.
pathOP.p = bestPath.p;
clear('i', 'bestPath');
