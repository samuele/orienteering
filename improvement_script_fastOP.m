% Solves the orienteering problem by performing the following algorithm:
% Step 1. Initialization
%     Perform initialization
%     Set record = team score of the initial solution
%     Set p = 10
%     Set deviation = p% * record
% Step 2. Improvement
%     For k = 1,2,... ,K
%         For i = 1,2 ..... I
%             Perform two-point exchange
%             Perform one-point movement
%             Perform clean up
%             If no movement has been made above, end I loop
%             If a new better solution has been obtained, then
%                 set record = score of new best solution
%                 set deviation = p% x record
%         End I loop
%         Perform reinitialization
%     End K loop
% Step 3. Reset p = 5, and redo Step 2 once more

moves = false;
improvement = false;

K = nReinitializationRuns;
I = nImprovementRuns;
p = reinitializationBound;

record = pathOP.p{1, 3};
deviation = p * record;

for counter = 1 : 2
    for k = 1 : K
        
        if (k ~= 1 && counter == 1) || (counter == 2 && k < K / 2)
            reinitialization_script;
        end
        
        for i = 1 : I
            %Sdisp(['counter = ' num2str(counter) ' K = ' num2str(k) ' I = ' num2str(i)]) %%% DEBUG %%%
            moves = false;
            improvement = false;
            
            two_point_exchange_script_fastOP;
            one_point_movement_script_fastOP;
            clean_up_script;
            
            if ~moves
                break
            end
            
            if improvement
                record = pathOP.p{1, 3};
                deviation = p * record;
            end
        end
    end
    
    p = 0.05;
end

% Clear workspace
clear('moves', 'improvement', 'k', 'K', 'i', 'I', 'p', 'record', 'deviation', 'counter');
