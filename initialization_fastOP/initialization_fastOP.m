function [pathOP, pathsNOP] = initialization_fastOP(G, si, sj, ei, ej, budget)

    pathOP = path;
    pathsNOP = path;

    S = [si, sj];
    E = [ei, ej];
    
    [R, C] = size(G);
    
    i = 1;
    
    % Compute points in the ellipse.
    intoEllipse = zeros(R * C, 4);
    for r = 1 : R
        for c = 1 : C
            if G(r, c) ~= 0 && ~isequal([r, c], S) && ~isequal([r, c], E) && dst([r, c], S) + dst([r, c], E) <= budget
                intoEllipse(i, :) = [r, c, G(r, c), dst([r, c], S) + dst([r, c], E)];
                i = i + 1;
            end
        end
    end
    
    intoEllipse(i : R * C, :) = [];
    
    % Initialization.
    adjList = computeAdjList(intoEllipse, S, E);
    
    % If the ellipse is empty, return the start-end path.
    if size(intoEllipse, 1) == 0
        base = cell(1, 3);
        base{1, 1} = [S; E];
        base{1, 2} = dst(S, E);
        base{1, 3} = G(S(1), S(2)) + G(E(1), E(2));
        
        pathOP.p = base;
        pathsNOP.p = cell(1, 3);
        return
    end
    
    L = min(nPathsNOP + 1, size(intoEllipse, 1));
    
    maxValue = 0;
    cost = intmax;
    maxIdx = 1;
    
    for l = 1 : L
        %disp(['round ' num2str(l) ' of ' num2str(L)]) %%% DEBUG %%%
        pathsNOP(l) = path;
        [lthPath, idx] = getLthSolution(G, intoEllipse, adjList, S, E, budget);
        
        intoEllipse(idx, 4) = -1;
        pathsNOP(l).p = lthPath;
        
        if pathsNOP(l).p{1, 3} > maxValue || (pathsNOP(l).p{1, 3} == maxValue && pathsNOP(l).p{1, 2} < cost) 
            maxValue = pathsNOP(l).p{1, 3};
            cost = pathsNOP(l).p{1, 2};
            maxIdx = l;
        end
    end
    
    pathOP.p = pathsNOP(maxIdx).p;
    pathsNOP(maxIdx) = [];

end

function [lthPath, idx] = getLthSolution(G, intoEllipse, adjList, S, E, budget)

    dim  = size(intoEllipse, 1);
    
    % Flags column.
    flags = zeros(dim, 1);
    
    % Index of the l-th more distant from start and end.
    idx = find(intoEllipse(:, 4) == max(intoEllipse(:, 4)), 1);
    
    % l-th solution partial results.
    results = cell(size(intoEllipse, 1), 3);
    
    results{1, 1} = [S; intoEllipse(idx, 1 : 2)];
    results{1, 2} = adjList(dim + 1, idx); 
    results{1, 3} = G(S(1, 1), S(1, 2)) + intoEllipse(idx, 3);
    flags(idx, 1) = 1;
    
    % Compute the nearest point to the last point of the path.
    last = idx;
    nearestToLast = computeNearestToLast(adjList, last, flags);

    while isAddable(last, nearestToLast, adjList, results{1, 2}, budget) 
        % Update.
        results{1, 1} = [results{1, 1}; intoEllipse(nearestToLast, 1 : 2)];
        results{1, 2} = adjList(last, nearestToLast) + results{1, 2};
        results{1, 3} = results{1, 3} + intoEllipse(nearestToLast, 3);
        flags(nearestToLast) = 1;
        
        % Compute the nearest point to the last point of the path.
        last = nearestToLast;
        nearestToLast = computeNearestToLast(adjList, last, flags);
    end
    
    % Update.
    results{1, 1} = [results{1, 1}; E];
    results{1, 2} = adjList(size(adjList, 1), last) + results{1, 2};
    results{1, 3} = results{1, 3} + G(E(1, 1), E(1, 2));

    % First path is now created.
    resIdx = 1;
    
    % Build others L - 1 paths in a greedy way until there exists a point
    % in the ellipse.
    while ~isempty(find(flags(:, 1) == 0, 1))
        % Next index.
        resIdx = resIdx + 1;
        
        % Initialization.
        results{resIdx, 1} = S;
        results{resIdx, 2} = 0;
        results{resIdx, 3} = G(S(1, 1), S(1, 2));
        
        % Compute the nearest point to the last point of the path.
        last = size(intoEllipse, 1) + 1;
        nearestToLast = computeNearestToLast(adjList, last, flags);
        
        while isAddable(last, nearestToLast, adjList, results{resIdx, 2}, budget)
            % Update the values of the resIdx-th path and remove the point
            % from the ellipse.
            results{resIdx, 1} = [results{resIdx, 1}; intoEllipse(nearestToLast, 1 : 2)];
            results{resIdx, 2} = adjList(last, nearestToLast) + results{resIdx, 2};
            results{resIdx, 3} = results{resIdx, 3} + intoEllipse(nearestToLast, 3);
            flags(nearestToLast) = 1;
        
            % Compute the nearest point to the last point of the path.
            last = nearestToLast;
            nearestToLast = computeNearestToLast(adjList, nearestToLast, flags);
        end
        
        % Update.
        results{resIdx, 1} = [results{resIdx, 1}; E];
        results{resIdx, 2} = adjList(size(adjList, 1), last) + results{resIdx, 2};
        results{resIdx, 3} = results{resIdx, 3} + G(E(1, 1), E(1, 2));
    end
    
    results(resIdx + 1 : size(results, 1), :) = [];
    
    % Find the best path.
    maxScoreIndex = find(cell2mat(results(:, 3)) == max(cell2mat(results(:, 3))));
    
    if(size(maxScoreIndex, 1) > 1)
        minCostIndex = maxScoreIndex(find(cell2mat(results(maxScoreIndex(:, 1), 2)) == min(cell2mat(results(:, 2)))));
        lthPath = results(minCostIndex(1), :); 
    else
        lthPath = results(maxScoreIndex, :);
    end
end

function [index] = computeNearestToLast(adjList, lastIdx, flags)
    
    cols = find(flags(:, 1) == 0);
    
    if isempty(cols)
        index = 0;
        return
    end
    
    index = cols(find(adjList(lastIdx, cols) == min(adjList(lastIdx, cols))), 1);

    if size(index, 1) > 1
        index = index(1, :);
    end
end


function [b] = isAddable(last, nearestToLast, adjList, offset, budget)
    b = false;
     
    if nearestToLast == 0
       return
    end
     
    if adjList(last, nearestToLast) + adjList(nearestToLast, size(adjList, 1)) + offset <= budget
        b = true;
    end
end

function [adjList] = computeAdjList(intoEllipse, S, E)

    dim = size(intoEllipse, 1);
    adjList = zeros(dim + 2);
    
    for i = 1 : dim + 2
        for j = i : dim + 2
            if i == j
                distance = intmax;
            elseif i == dim + 1
                if j == dim + 2
                    distance = dst(S, E);
                else
                    distance = dst(S, intoEllipse(j, 1 : 2));
                end
            elseif i == dim + 2
                if j == dim + 1
                    distance = dst(S, E);
                else
                    distance = dst(E, intoEllipse(j, 1 : 2));
                end
            else
                if j == dim + 1
                    distance = dst(intoEllipse(i, 1 : 2), S);
                elseif j == dim + 2
                    distance = dst(intoEllipse(i, 1 : 2), E);
                else
                    distance = dst(intoEllipse(i, 1 : 2), intoEllipse(j, 1 : 2));
                end
            end
            adjList(i, j) = distance;
            adjList(j, i) = distance;
        end
    end
end
