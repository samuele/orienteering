from __future__ import print_function
import glob, os, sys
import matlab.engine
import time

# Run this script in folder ../orienteering/orienteeringInstancesUniform

fastOP_stat = {'Points': [], 'Scores': [], 'Costs': [], 'Times': []}
New_stat = {'Points': [], 'Scores': [], 'Costs': [], 'Times': []}

print('Starting MATLAB engine and configure the environment...', end = '')
eng = matlab.engine.start_matlab()
eng.cd('..')
eng.config(nargout = 0)
eng.cd('orienteeringInstancesUniform')
print(' Done.')

cnt = -1
files = glob.glob('*.mat')
for file in files:
    cnt += 1
    print('{0:.0f}%'.format(float(cnt) / len(files) * 100))

    print('Loading environment...', end = '')
    eng.load(file, nargout = 0)
    print(' Done.')

    print('Drawing the model...', end = '')
    eng.assignin('base', 'orienteeringInstanceTitle', file, nargout = 0)
    eng.draw_matrix(nargout = 0)
    print(' Done.')

    print('Running fastOP algorithm...', end = '')
    eng.cd('..')
    start = time.time()
    output_fastOP = eng.solveOrienteeringWithFastOP(eng.eval('matrixATX', nargout = 1),
                                                    eng.eval('startNode', nargout = 1),
                                                    eng.eval('endNode', nargout = 1),
                                                    eng.eval('budget', nargout = 1),
                                                    eng.eval('unitOfDistance', nargout = 1), nargout = 4)
    end = time.time()
    print(' Done.')

    fastOP_stat['Points'].append(int(eng.len(output_fastOP[3], nargout = 1)))
    fastOP_stat['Scores'].append(eng.score(output_fastOP[3], nargout = 1))
    fastOP_stat['Costs'].append(eng.cost(output_fastOP[3], nargout = 1))
    fastOP_stat['Times'].append(end - start)

    print('Drawing path on the model...', end = '')
    eng.draw_path(output_fastOP[3], 'fastOP', nargout = 0)
    print(' Done.')

    print('Running New algorithm...', end = '')
    start = time.time()
    output_New = eng.solveOrienteering(eng.eval('matrixATX', nargout = 1),
                                       eng.eval('startNode', nargout = 1),
                                       eng.eval('endNode', nargout = 1),
                                       eng.eval('budget', nargout = 1),
                                       eng.eval('unitOfDistance', nargout = 1), nargout = 4)
    end = time.time()
    print(' Done.')

    New_stat['Points'].append(int(eng.len(output_New[3], nargout = 1)))
    New_stat['Scores'].append(eng.score(output_New[3], nargout = 1))
    New_stat['Costs'].append(eng.cost(output_New[3], nargout = 1))
    New_stat['Times'].append(end - start)

    print('Drawing path on the model...', end = '')
    eng.draw_path(output_New[3], 'New', nargout = 0)
    print(' Done.')

    print('Saving figure and closing...', end = '')
    eng.save_and_close('./orienteeringInstancesUniform/' + os.path.splitext(file)[0] + '.png', nargout = 0)
    print(' Done.')

    eng.clear(nargout = 0)
    eng.cd('./orienteeringInstancesUniform')

print('100%\n\nStats generation...')

time.sleep(3)

percScores = []
percCosts = []

print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
print('+       Instance       ||   Points    ||       Score       ||       Cost        ||       Time        || Improvement (Score) || Improvement (Cost) +')
print('+-------------------------------------------------------------------------------------------------------------------------------------------------+')
for i in range(0, cnt + 1):
    print('+ {:>20} '.format(files[i]), end = '')
    print('|| {:4d}   {:4d} '.format(fastOP_stat['Points'][i], New_stat['Points'][i]), end = '')
    print('|| {:7.2f}   {:7.2f} '.format(fastOP_stat['Scores'][i], New_stat['Scores'][i]), end = '')
    print('|| {:7.2f}   {:7.2f} '.format(fastOP_stat['Costs'][i], New_stat['Costs'][i]), end = '')
    print('|| {:7.2f}   {:7.2f} '.format(fastOP_stat['Times'][i], New_stat['Times'][i]), end = '')

    if fastOP_stat['Scores'][i] != 0:
        percScore = ((New_stat['Scores'][i] - fastOP_stat['Scores'][i]) / fastOP_stat['Scores'][i]) * 100
        percScores.append(percScore)
        print('||      {:+7.2f}%       '.format(percScore), end = '')
    else:
        print('||          +inf       ', end = '')

    if fastOP_stat['Costs'][i] != 0:
        percCost = ((New_stat['Costs'][i] - fastOP_stat['Costs'][i]) / fastOP_stat['Costs'][i]) * 100
        percCosts.append(percCost)
        print('||      {:+7.2f}%      +'.format(percCost))
    else:
        print('||         +inf       +', end = '')

print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

print('\nFinal results generation...')
time.sleep(3)

print('+-------------------------------------+')
if len(percScores) != 0:
    avgscore = sum(percScores) / len(percScores)
    print('+ Avg. score improvement     {:+7.2f}% +'.format(avgscore))
else:
    print('+ Avg. score improvement              +')

if len(percCosts) != 0:
    avgcost = sum(percCosts) / len(percCosts)
    print('+ Avg. cost improvement      {:+7.2f}% +'.format(avgcost))
else:
    print('+ Avg. cost improvement               +')

if len(New_stat['Times']) != 0:
    avgtime = sum(New_stat['Times']) / len(New_stat['Times'])
    print('+ Avg. execution time        {:7.2f}s +'.format(avgtime))
else:
    print('+ Avg. execution time                 +')
print('+-------------------------------------+')
